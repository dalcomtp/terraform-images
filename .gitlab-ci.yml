variables:
  STABLE_VERSION: "1.0"
  STABLE_IMAGE_NAME: "$CI_REGISTRY_IMAGE/stable:latest"
  BASE_IMAGE: "alpine:3.13.1"
  BUILD_IMAGE_NAME: "$CI_REGISTRY_IMAGE/branches/$CI_COMMIT_REF_SLUG-$TERRAFORM_VERSION:$CI_COMMIT_SHA"
  RELEASE_IMAGE_NAME: "$CI_REGISTRY_IMAGE/releases/$TERRAFORM_VERSION"
  TF_ADDRESS: "$CI_API_V4_URL/projects/$CI_PROJECT_ID/terraform/state/$CI_PIPELINE_IID-$STATE_NAME"

.versions:
  parallel:
    matrix:
      - TERRAFORM_BINARY_VERSION: "1.0.0"
        TERRAFORM_VERSION: "1.0"
        STATE_NAME: terraform10
      - TERRAFORM_BINARY_VERSION: "0.15.5"
        TERRAFORM_VERSION: "0.15"
        STATE_NAME: terraform015
      - TERRAFORM_BINARY_VERSION: "0.14.11"
        TERRAFORM_VERSION: "0.14"
        STATE_NAME: terraform014
      - TERRAFORM_BINARY_VERSION: "0.13.7"
        TERRAFORM_VERSION: "0.13"
        STATE_NAME: terraform013

stages:
  - lint
  - build
  - test-init
  - test-validate
  - test-plan
  - test-apply
  - test-destroy
  - prepare-release
  - release

shell check:
  stage: lint
  image: koalaman/shellcheck-alpine:stable
  before_script:
    - shellcheck --version
  script:
    - shellcheck src/**/*.sh

dockerfile check:
  stage: lint
  image: hadolint/hadolint:latest-alpine
  before_script:
    - hadolint --version
  script:
    - hadolint Dockerfile

.dind:
  services:
    - docker:20.10.6-dind
  image: docker:20.10.6
  before_script:
    - docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" "$CI_REGISTRY"

build:
  extends:
    - .dind
    - .versions
  stage: build
  script:
    - docker image build --tag "$BUILD_IMAGE_NAME" --build-arg BASE_IMAGE=$BASE_IMAGE --build-arg TERRAFORM_BINARY_VERSION=$TERRAFORM_BINARY_VERSION .
    - docker image push "$BUILD_IMAGE_NAME"

.test:
  image: "$BUILD_IMAGE_NAME"
  before_script:
    - gitlab-terraform version
    - jq --version
    - cd tests
  cache:
    key: "$TERRAFORM_VERSION-$CI_COMMIT_REF_SLUG"
    paths:
      - tests/.terraform/

test-init:
  extends:
    - .test
    - .versions
  stage: test-init
  script:
    - export DEBUG_OUTPUT=true
    - gitlab-terraform init

test-validate:
  extends:
    - .test
    - .versions
  stage: test-validate
  script:
    - gitlab-terraform validate

test-plan:
  extends:
    - .test
    - .versions
  stage: test-plan
  script:
    - gitlab-terraform plan
    - if [[ ! -f "plan.cache" ]]; then echo "expected to find a plan.cache file"; exit 1; fi
    - gitlab-terraform plan-json
    - if [[ ! -f "plan.json" ]]; then echo "expected to find a plan.json file"; exit 1; fi
    - mv plan.cache $TERRAFORM_VERSION-plan.cache
  artifacts:
    paths:
      - "tests/*-plan.cache"

test-apply:
  extends:
    - .test
    - .versions
  stage: test-apply
  script:
    - mv $TERRAFORM_VERSION-plan.cache plan.cache
    - gitlab-terraform apply

test-destroy:
  extends:
    - .test
    - .versions
  stage: test-destroy
  script:
    - gitlab-terraform destroy

release:
  extends:
    - .dind
    - .versions
  stage: release
  script:
    - docker image pull "$BUILD_IMAGE_NAME"
    - docker image tag "$BUILD_IMAGE_NAME" "$RELEASE_IMAGE_NAME:latest"
    - docker image tag "$BUILD_IMAGE_NAME" "$CI_REGISTRY_IMAGE/releases/terraform:$TERRAFORM_BINARY_VERSION"
    - docker image tag "$BUILD_IMAGE_NAME" "$RELEASE_IMAGE_NAME:$CI_COMMIT_TAG"
    - docker image push "$RELEASE_IMAGE_NAME:latest"
    - docker image push "$CI_REGISTRY_IMAGE/releases/terraform:$TERRAFORM_BINARY_VERSION"
    - docker image push "$RELEASE_IMAGE_NAME:$CI_COMMIT_TAG"
    - if [ "$TERRAFORM_VERSION" = "$STABLE_VERSION" ]; then docker image tag "$BUILD_IMAGE_NAME" "$STABLE_IMAGE_NAME"; fi
    - if [ "$TERRAFORM_VERSION" = "$STABLE_VERSION" ]; then docker image push "$STABLE_IMAGE_NAME"; fi
  only:
    - tags

.semantic-release:
  image: node:12-buster-slim
  stage: prepare-release
  before_script:
    - apt-get update && apt-get install -y --no-install-recommends git-core ca-certificates
    - npm install -g semantic-release @semantic-release/gitlab
  script:
    - semantic-release $DRY_RUN_OPT -b $CI_COMMIT_REF_NAME

tag_release-dryrun:
  extends: .semantic-release
  variables:
    DRY_RUN_OPT: "-d"
  only:
    refs:
      - branches@gitlab-org/terraform-images
  except:
    refs:
      - master

tag_release:
  extends: .semantic-release
  only:
    refs:
      - master@gitlab-org/terraform-images
